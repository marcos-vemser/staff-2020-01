package br.com.dbccompany.bancodigital.Entity;

import sun.tools.jar.CommandLine;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contas {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue( generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private Integer numero;
    private Double saldo;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_agencia")
    private Agencias agencias;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_tipo_conta")
    private TipoConta tpConta;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "clientes_x_contas",
            joinColumns = { @JoinColumn( name = "id_conta" ) },
            inverseJoinColumns = { @JoinColumn( name = "id_cliente" ) })
    private List<Clientes> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Agencias getAgencias() {
        return agencias;
    }

    public void setAgencias(Agencias agencias) {
        this.agencias = agencias;
    }

    public TipoConta getTpConta() {
        return tpConta;
    }

    public void setTpConta(TipoConta tpConta) {
        this.tpConta = tpConta;
    }

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }
}
