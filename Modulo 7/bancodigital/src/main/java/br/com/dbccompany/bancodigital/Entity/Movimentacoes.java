package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Movimentacoes {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "MOV_SEQ", sequenceName = "MOV_SEQ")
    @GeneratedValue( generator = "MOV_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private Double valor;

    @Enumerated(EnumType.STRING)
    private TipoMovimentacao tipoMov;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_conta")
    private Contas conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TipoMovimentacao getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(TipoMovimentacao tipoMov) {
        this.tipoMov = tipoMov;
    }

    public Contas getConta() {
        return conta;
    }

    public void setConta(Contas conta) {
        this.conta = conta;
    }
}
