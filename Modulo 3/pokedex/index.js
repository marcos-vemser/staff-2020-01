let pokeApi = new PokeApi();

async function buscar() {
    let pokemonEspecifico = await pokeApi.buscar(110);
    let poke = new Pokemon( pokemonEspecifico );
    renderizacaoPokemon( poke );
}

function renderizacaoPokemon( pokemon ) {
    let dadosPokemon = document.getElementById('dadosPokemon');
    let nome = dadosPokemon.querySelector('.nome');
    //let imgPokemon = dadosPokemon.querySelector('.thumb');
    nome.innerHTML = pokemon.nome;
    //imgPokemon.src = pokemon.sprites.front_default
}

buscar();