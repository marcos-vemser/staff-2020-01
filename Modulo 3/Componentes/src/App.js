import React, { Component } from 'react';
import './App.css';

//import CompA, { CompB } from './components/ExemploComponenteBasico';
import Membros from './components/Membros';
import Teste from './components/Teste';

class App extends Component {
  constructor( props ) {
    super( props );
  }

  render() {
    return (
      <div className="App">
        <Membros nome="João" sobrenome="Silva" />
        <Membros nome="Maria" sobrenome="Silva" />
        <Membros nome="Pedro" sobrenome="Silva" />
        <Teste>
          Aqui
        </Teste>
        {/* <CompA />
        <CompA />
        <CompB />
        <CompA />
        <CompA />
        <CompA /> */}
      </div>
    );
  }
}

export default App;