function multiplicar (mult, ...valores){
    return valores.map( valor => mult * valor);
}

//console.log(multiplicar(5,3,4));

function validacao(e){
    let target = e.target;
    let msg = '';
    switch (target.name) {
        case "nome":
            if(target.value == ''){
                msg = "Esse campo não pode ser vazio";
            }else{
                msg = '';
            }  
            break;
        case "email":
            break;
        default:
            break;
    }
    let erro = document.getElementById(`msg-erro-${target.name}`)
    erro.innerHTML = msg;
}

let vazio = document.getElementsByClassName('vazio');
for (let i = 0; i < vazio.length; i++) {
    vazio[i].addEventListener('blur', (e) => validacao(e));
}