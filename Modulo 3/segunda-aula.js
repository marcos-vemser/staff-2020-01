function calcularCirculo({ raio, tipoCalculo:tipo }){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio);
}

let circulo = {
    raio: 3,
    tipoCalculo: "A"
}

//console.log(calcularCirculo(circulo));

/* function bissexto(ano){
    return (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? true : false; 
} */

const teste = {
    diaAula: "Segundo",
    local: "DBC",
    bissexto(ano) {
        return (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? true : false; 
    }

}

let bissexto = ano => (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0); 

//console.log(bissexto(2015));
//console.log(teste.bissexto(2015));
//console.log(bissexto(2016));

function somarPares(numeros) {
    let resultado = 0;
    for (let i = 0; i < numeros.length; i++) {
        if (i % 2 === 0) {
            resultado += numeros[i];
        }
    }
    return resultado;
}

//console.log(somarPares([1, 56, 4.34, 6 , -2]));

/* function adicionar(op1){
    return function(op2){
        return op1 + op2;
    }
} */

let adicionar = op1 => op2 => op1 + op2;

//console.log(adicionar(3)(4));
//console.log(adicionar(5642)(8749));

/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));